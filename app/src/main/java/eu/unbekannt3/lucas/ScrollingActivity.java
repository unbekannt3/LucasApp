package eu.unbekannt3.lucas;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class ScrollingActivity extends AppCompatActivity {

    private boolean stop = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        startSpamThread();
    }

    @Override
    public void onPause() {
        super.onPause();
        stop = true; // Stoppt Texterzeugung bei Verlassen der App
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Toast.makeText(ScrollingActivity.this, "Es gibt hier nichts zum Einstellen du Bob!", Toast.LENGTH_LONG).show();
            return true;
        }
        if (id == R.id.action_about) {
            //AlertBox Builder
            final AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
            //Nachricht & Titel
            alertbox.setMessage(getResources().getString(R.string.about_nachricht)).setTitle(getResources().getString(R.string.about_titel));
            //Schließen Button
            alertbox.setNeutralButton("Interessiert mich nicht", new DialogInterface.OnClickListener() {
                // Klicklistener
                public void onClick(DialogInterface arg0, int arg1) {

                }
            });
            //Rendern
            alertbox.show();
        }
        return super.onOptionsItemSelected(item);
    }

    private void startSpamThread() {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            public void run() {
                while (!stop) {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.post(new Runnable() {
                        public void run() {
                            TextView outputText = (TextView) findViewById(R.id.content_TEXT);
                            outputText.setMovementMethod(new ScrollingMovementMethod());
                            outputText.append("\n" + getString(R.string.LUCAS));
                        }
                    });
                }
            }
        };
        new Thread(runnable).start();
    }

}
